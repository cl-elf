
(in-package :cl-elf)


(defun mask-predicate (value mask)
  (not (zerop (logand value mask))))


(defmacro slot->flags (object slot predicate  &rest descriptions)
  (let ((slot-value-sym (gensym)))
    `(let ((,slot-value-sym (slot-value ,object ,slot)))
       (remove-nils
        (list
         ,@(mapcar
            #'(lambda (description)
                `(if
                  (funcall ,predicate ,slot-value-sym ,(car description))
                  ,@(cdr description)))
            descriptions))))))

(defgeneric describe-elf-file-header (elf-file))

(defmethod describe-elf-file-header ((elf elf-file))
  (let 
      ((elf-header (header-of elf)))
    (format t "File Type : ~A~%"
            (slot->flags elf-header 'e-type #'=
                               (+ET_NONE+ "None")
                               (+ET_REL+ "Relocatable")
                               (+ET_EXEC+ "Executable")
                               (+ET_DYN+ "Shared")
                               (+ET_CORE+ "Core")
                               (+ET_LOPROC+ "CPU Specific")
                               (+ET_HIPROC+ "CPU Specific")))
    (format t "Machine Type : ~A~%"
            (slot->flags elf-header 'e-machine #'=
                               (+EM_NONE+ "None")
                               (+EM_M32+ "Motorola 32")
                               (+EM_SPARC+ "SPARC")
                               (+EM_386+ "Intel x86")
                               (+EM_68K+ "M68k")
                               (+EM_88k+ "88k")
                               (+EM_486+ "Intel x86?")
                               (+EM_860+ "Intel 860")
                               (+EM_MIPS+ "Mips R3000")
                               (+EM_MIPS_RS4_BE+ "Mips R4000 big-endian")
                               (+EM_PARISC+ "HPPA")
                               (+EM_SPARC32PLUS+ "Suns v8Plus")
                               (+EM_PPC+ "Power PC")
                               (+EM_PPC64+ "Power PC64")))))

