;;;; Silly emacs, this is -*- Lisp -*-

(in-package :asdf)

;; to do -- elf header should possibly be elf types
(defsystem :cl-elf
    :name "cl-elf"
    :author "John Connors"
    :version "1.0"
    :licence "MIT"
    :description "Code for reading and writing elf files"
    :depends-on (:iterate)
    :serial t
    :components ((:file "package")		 
                 (:file "elf-rw")
                 (:file "elf-header")                 
                 (:file "elf-file")))

;;                  (:file "elf-file-header"
;;                         :depends-on ("package" "elf-rw" "elf-header" 
;;                                                "elf-file"))
;;                  (:file "elf-section-types"
;;                         :depends-on ("package" 
;;                                      "elf-rw" "elf-header"
;;                                      "elf-file-header" "elf-file"))))
    