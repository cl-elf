
(in-package :cl-elf)

(declaim (optimize (debug 3)
                   (safety 3)
                   (compilation-speed 0)
                   (speed 0)))

;; -- handy things ----------------------------------------------------------

(defmacro once-only ((&rest names) &body body)
  (let ((gensyms (loop for n in names collect (gensym))))
    `(let (,@(loop for g in gensyms collect `(,g (gensym))))
      `(let (,,@(loop for g in gensyms for n in names collect ``(,,g ,,n)))
        ,(let (,@(loop for n in names for g in gensyms collect `(,n ,g)))
           ,@body)))))

(defmacro with-gensyms ((&rest names) &body body)
  `(let ,(loop for n in names collect `(,n (gensym)))
     ,@body))

(defun remove-nils (lst)
  "eliminate elements where nil"
  (let ((acc nil))
    (dolist (x lst (nreverse acc))
      (when x (push x acc)))))


(defgeneric section-of (elf index))

(defgeneric (setf section-of) (value elf index))

(defgeneric offset-of (section))

(defgeneric entry-size-of (section))

(defgeneric size-of (section))

(defgeneric elf-type-of (section))

(defgeneric elf-entry-type-of (section))

(defgeneric elf-entry-type-class-of (section))

(defgeneric get-entry (section  &key elf-file index offset)
  (:documentation "Extract an entry from a section by index or offset"))

(defgeneric get-offset-from-index (section index))

(defgeneric make-rel-entry (elf-rel-info-entry &key section elf-file))

(defgeneric elf-section-name (elf index))

;; -- elf-file -------------------------------------------------------------

;; higher level structure for an elf file
(defclass elf-file (buffered-data-mixin)
  ((header :accessor header-of :initform nil
           :documentation "Header of file")
   (sections :accessor sections-of :initform 
             (make-array 0 :adjustable t :fill-pointer 0))))

;; a few convienence methods to save needless repetition


(defmacro buffered-data-excursion ((buffer pos) &rest body)
  "Ecexcute body, with buffered-pos-of elf at pos then restore it"
  (once-only (buffer)
    (with-gensyms (old-pos)
      `(let*
         ((,old-pos (buffer-pos-of ,buffer)))
         (setf 
          (buffer-pos-of ,buffer) ,pos)
         (prog1
             (progn ,@body)
           (setf 
            (buffer-pos-of ,buffer) ,old-pos))))))

(defmethod section-of ((elf elf-file)  index)
  (elt (sections-of elf) index))

(defmethod (setf section-of) (value (elf elf-file) index)
  (setf (elt (sections-of elf) index) value))
           
     
;; -- elf-section -------------------------------------------------------------
    
(defclass elf-section (buffered-data-mixin)
  ((header :accessor header-of :initform 0)
   (name :initform nil :accessor name-of))
   (:documentation "Object that represents a generic elf section"))


(defmethod offset-of ((section elf-section))
  "Return the offset to the actual section data in the file."
  (sh-offset-of  (header-of section)))

(defmethod entry-size-of ((section elf-section))
  (sh-entsize-of (header-of section)))

(defmethod size-of ((section elf-section))
  (sh-size-of (header-of section)))

(defmethod initialize-instance :after ((section elf-section) &key elf index)
  (check-type elf elf-file)
  (let* ((elf-header (header-of elf))
         (section-header-offset
          (+ (e-shoff-of elf-header)
             (* index (e-shentsize-of elf-header)))))
    ;; move buffer pointer to start of section header
    (setf (buffer-pos-of elf)
          section-header-offset)
    ;; read in the section header
    (setf (header-of section)
          (read-value 'elf32-section-header elf))
    ;; grab the actual section itself now we know where it is via header
    (setf (buffered-data-of section)
                     (subseq (buffered-data-of elf) 
                             (offset-of section)
                             (+ (offset-of section) 
                                (size-of section))))))


(defmethod elf-type-of ((section elf-section))
  (sh-type-of (header-of section)))

(defclass elf-string-section (elf-section)
  () 
  (:documentation "Specialist class for elf string section"))

(defclass elf-symbol-section (elf-section)
  ()
  (:documentation "Specialist class for elf symbol section"))

(defclass elf-relinfo-section (elf-section)
  ()
  (:documentation "Specialist class for elf relocation info section"))

(defclass elf-relainfo-section (elf-section) 
  ()
  (:documentation "Speciallist class for elf address relocation info section"))


;; -entries -----------------------------------------------------------------------------


(defmethod elf-entry-type-of ((section elf-section))
  "Given the section, return the type used to read an entry from it via read-value"
  (let
      ((section-entry-types
        (list (cons +SHT_NULL+  'u8)
         (cons +SHT_STRTAB+  'asciiz)
         (cons +SHT_PROGBITS+  'u8)
         (cons +SHT_NOTE+  'asciiz)
         (cons +SHT_SYMTAB+  'elf-sym)
         (cons +SHT_REL+  'elf-rel)
         (cons +SHT_RELA+  'elf-rela))))
    (or
     (cdr (assoc (elf-type-of section) section-entry-types))
     'u8)))


(defmethod elf-entry-type-class-of ((section elf-section))
  "Given the section, return the class that should be used to represent that section"
  (let
      ((section-entry-classes
        (list (cons +SHT_NULL+ 'elf-section)
         (cons +SHT_STRTAB+  'elf-string-section)
         (cons +SHT_PROGBITS+  'elf-section)
         (cons +SHT_NOTE+ 'elf-string-section)
         (cons +SHT_SYMTAB+ 'elf-symbol-section)
         (cons +SHT_REL+ 'elf-relinfo-section)
         (cons +SHT_RELA+ 'elf-relainfo-section))))
    (or
     (cdr (assoc (elf-type-of section) section-entry-classes))
     'elf-section)))


(defmethod get-offset-from-index ((section elf-section)  index)
  "Given an index into a section calclate the offset it represents."
  (* index (entry-size-of section)))

(defmacro with-real-offset (real-offset-sym (section index offset) &rest body)
  "Use real-offset-sym to represent the actual offset represented by
either index or offset into the section in body."
  (once-only (index)
    `(let* ((,real-offset-sym 
             (if ,index
                 (* ,index (entry-size-of ,section))                 
                 ,offset)))
       ,@body)))

;; -- exctrating entries from sections ---------------------------------------------

   
(defmethod get-entry ((section elf-section) &key elf-file index offset) 
  "Retrieve an entry from an elf-section via either an index or an offset."
  (declare (ignore elf-file))
  (assert (or (null offset) (null index)))
  (assert (or offset index))
  ;; this will fire if you try to index an unindexable section
  (assert (or (null index) 
	      (and index 
		   (not (zerop (entry-size-of section))))))
  (with-real-offset real-offset 
    (section index offset)  
    (buffered-data-excursion 
     (section real-offset)
         (read-value 
          (elf-entry-type-of section)
          section))))

;; -- strings from string sections ---------------------------------------------------
   
(defmethod get-entry ((section elf-string-section) &key elf-file index offset)
  "Retrieve an string from an appropiate section via an offset."  
  (call-next-method section :elf-file elf-file :index index :offset offset)
  ;; you can't index these
  (assert (and (integerp offset) (null index)))
  (buffered-data-excursion
   (section offset)
    (read-value 'asciiz section)))

(defun make-entry-extractor (elf &key section-index)
  "Create and return a function to extract an entry from the given section."
  (lambda (offset)
    (get-entry (section-of elf section-index) :elf-file elf :offset offset)))

 ;; -- symbol entries from symbol sections  -----------------------------------------

(defclass elf-symbol ()
  ((name :initarg :name :reader name-of) 
   (entry :initarg :entry :reader entry-of)))

(defmethod get-entry ((section elf-symbol-section) &key elf-file index offset)
  "Extract a symbol from a symbol section."
  (let* ((elf-symbol (call-next-method section :elf-file elf-file :index index :offset offset))
         (string-extractor
          (make-entry-extractor 
           elf-file 
           :section-index (sh-link-of (header-of section)))))
    (make-instance 'elf-symbol
                   :name 
                   (funcall string-extractor (st-name-of elf-symbol))
                   ;; to do - things with value, size, etc?
                   :entry
                   elf-symbol)))


;; -- relocation info entries from relocation info sections -------------------------

(defclass elf-rel-info ()
  ((rel-section-index :accessor rel-section-index-of 
                      :initarg :rel-section-index
                      :documentation "Index of section this relocation affects.")
   (offset :accessor offset-of 
           :initarg :offset
           :documentation "Offset into section where relocation is applied")
   (relocation-type :accessor relocation-type-of 
                    :initarg :relocation-type 
                    :documentation "Type of relocation to apply")
   (symbol :accessor symbol-of 
           :initarg :symbol
           :documentation "Symbol associated with this relocation.")))


(defmethod make-rel-entry ((elf-rel-info-entry elf-rel-info) &key section elf-file)
  (labels
      ((rel-type (rel-entry)
         (logand (r-info-of rel-entry) #XFF))
       (rel-sym (rel-entry)
         (ash (r-info-of rel-entry) -8)))
    (make-instance 'elf-rel-info
                   :rel-section-index (sh-info-of (header-of section))
                   :offset (r-offset-of elf-rel-info-entry)
                   :symbol (get-entry 
                            (section-of elf-file 
                                        (sh-link-of (header-of section)))
                            :index (rel-sym elf-rel-info-entry))
                   :type (rel-type elf-rel-info-entry))))

;; -- relocation info entries with addends from relocation info sections -----------------

(defmethod get-entry ((section elf-relinfo-section)  &key elf-file index offset)
  "Extract a relocation symbol from a relocation section"
  (let ((elf-rel-info-entry 
         (call-next-method section :elf-file elf-file 
                                   :index index 
                                   :offset offset)))
    (make-rel-entry elf-rel-info-entry 
                    :elf-file elf-file
                    :section section)))


(defclass elf-rela-info (elf-rel-info)
  ((addend 
    :accessor addend-of
    :initarg :addend))
  (:documentation "Represents a relocation info record with an addend"))

(defmethod update-instance-for-different-class :after ((old elf-rel-info)
                                                        (new elf-rela-info)
                                                       &key addend)
  "Convert a plain relocation record into one with an addend."
  (setf (addend-of new) addend))

(defmethod make-rel-entry ((rel-entry elf-rela-info) &key section elf-file)
  (let ((result (call-next-method rel-entry :section section :elf-file elf-file)))
    (change-class result 'elf-rela-info :addend (r-addend-of result))))

(defmethod get-entry ((section elf-relainfo-section)  &key elf-file index offset)
  "Extract a relocation symbol from a relocation section with addends"
  (let* ((elf-rela-info-entry 
         (call-next-method section :elf-file elf-file 
                                   :index index 
                                   :offset offset))
         (result (make-rel-entry elf-rela-info-entry 
                                 :elf-file elf-file
                                 :section section)))
    result))



;; --- extract name of a section --------------------------------------------------


(defmethod elf-section-name ((elf elf-file) index)
  "Return the name of a section, given the index."
  (let 
      ((string-section-index (e-shstrndx-of (header-of  elf))))
   (get-entry  (aref (sections-of elf) string-section-index) 
               :offset (sh-name-of (header-of (aref (sections-of elf) index))))))
               
      
;; actually read an elf file --------------------------------------------------------


(defmethod initialize-instance :after ((elf elf-file) &key filepath)
  "Actually read an elf file and process the sections."
    (labels
        ((read-header ()
           "Read and set the elf file header"
           (setf (buffer-pos-of elf) 0)
           (setf (header-of elf)
                 (read-value 'elf-header elf)))

         ;; create n empty sections for each section in the elf file
         (create-sections (section-count)
           "Create the sections in the elf file, specialising them as we go along."
           (loop
              for section-index from 0 below section-count
              do (let 
                     ((section (make-instance 'elf-section :elf elf :index section-index)))
                   (change-class section (elf-entry-type-class-of section))
                   (vector-push-extend section (sections-of elf)))))         

         (set-section-names () 
           (loop 
              for section-index from 0 below (e-shnum-of (header-of elf))
              do (let ((section (section-of elf section-index))) 
                   (setf (name-of section)
                         (elf-section-name elf section-index))))))
    
      
      ;; defmethod read-elf file
      (read-buffered-data elf :filepath filepath)
      (read-header)
      (create-sections (e-shnum-of (header-of elf)))))


