
(in-package :cl-elf)

(defmethod section-type->description ((section elf-section))
  (let
      ((section-header (header-of section)))
    (slot->flags section-header 'sh-type #'=
                 (+SHT_PROGBITS+ "Program Data")
                 (+SHT_SYMTAB+ "Symbol Table")
                 (+SHT_STRTAB+ "String Table")
                 (+SHT_RELA+ "Relocation entries")
                 (+SHT_HASH+ "Hash table")
                 (+SHT_DYNAMIC+ "Dynamic Linking info")
                 (+SHT_NOBITS+  "Program space with no data (bss)")
                 (+SHT_NOTE+ "Notes")
                 (+SHT_REL+ "Relocation entries, no addednd")
                 (+SHT_SHLIB+ "Reserved")
                 (+SHT_DYNSYM+ "Dynamic linker symbol table")
                 (+SHT_INIT_ARRAY+ "Constructor Array")
                 (+SHT_FINI_ARRAY+ "Destructor Array")
                 (+SHT_PREINIT_ARRAY+ "Pre-constructor array+")
                 (+SHT_GROUP+ "Section Group")
                 (+SHT_SYMTAB_SHNDX+ "Extended section indices"))))


(defmethod section-type->keyword ((section elf-section))
  (let 
      ((section-header (header-of section)))
    (car
     (slot->flags section-header 'sh-type #'=
                  (+SHT_PROGBITS+ :program-data)
                  (+SHT_SYMTAB+ :symbol-table)
                  (+SHT_STRTAB+ :string-table)
                  (+SHT_RELA+ :relocation-entries)
                  (+SHT_HASH+ :hash-table)
                  (+SHT_DYNAMIC+ :dynamic-linking-info)
                  (+SHT_NOBITS+  :program-space)
                  (+SHT_NOTE+  :notes)
                  (+SHT_REL+ :relocation-entries-no-addend)
                  (+SHT_SHLIB+ :reserved)
                  (+SHT_DYNSYM+ :dynamic-linker-symbol-table)
                  (+SHT_INIT_ARRAY+ :constructor-array)
                  (+SHT_FINI_ARRAY+ :destructor-array)
                  (+SHT_PREINIT_ARRAY+ :pre-constructor-array)
                  (+SHT_GROUP+ :segment-group)
                  (+SHT_SYMTAB_SHNDX+ :extended-section-indices)))))
  



(defmethod section-flags->keywords ((section elf-section))
  (let
      ((section-header (header-of section)))    
    (slot->flags section-header 'sh-flags #'mask-predicate
                 (+SHF_WRITE+ :writeable)
                 (+SHF_ALLOC+ :allocate-memory)
                 (+SHF_EXECINSTR+ :executable)
                 (+SHF_MERGE+ :possibly-merged)
                 (+SHF_STRINGS+ :strings)
                 (+SHF_INFO_LINK+ :sht-index)
                 (+SHF_LINK_ORDER+ :preserve-link-order)
                 (+SHF_OS_NONCONFORMING+ :non-standard)
                 (+SHF_GROUP+ :group-member)
                 (+SHF_TLS+ :thread-local-storage))))
  
(defmethod section-flags->descriptions ((section elf-section))
  (slot->flags section-header 'sh-flags #'mask-predicate
               (+SHF_WRITE+ "writeable")
               (+SHF_ALLOC+ "occupies memory")
               (+SHF_EXECINSTR+ "executable")
               (+SHF_MERGE+ "possibly merged")
               (+SHF_STRINGS+ "strings")
               (+SHF_INFO_LINK+ "index")
               (+SHF_LINK_ORDER+ "preserve link order")
               (+SHF_OS_NONCONFORMING+ "non standard")
               (+SHF_GROUP+ "group member")
               (+SHF_TLS+ "thread local storage")))



