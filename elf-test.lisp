;;;; Silly emacs, this is -*- Lisp -*-

;; TO DO -- relocation entries


(in-package #:cl-elf)

(defparameter *test-elf-file*
  (make-instance 'elf-file                  
                 :filepath  (merge-pathnames #P"main.o"))) 


(e-shstrndx-of (header-of *test-elf-file*))

(format t "Elf file has ~D sections." (length (sections-of *test-elf-file*)))


(loop for section across (sections-of *test-elf-file*)
     do (format t "Section type ~X ~A ~A ~A (~A) is ~X bytes long~%" 
                (sh-type-of (header-of section))
                (type-of section)
                (elf-type-of section)
                (elf-entry-type-class-of section)
                (elf-entry-type-of section)
                (size-of section)))

;; test code

;; ELF Header:
;;   Magic:   7f 45 4c 46 01 01 01 00 00 00 00 00 00 00 00 00
;;   Class:                             ELF32
;;   Data:                              2's complement, little endian
;;   Version:                           1 (current)
;;   OS/ABI:                            UNIX - System V
;;   ABI Version:                       0
;;   Type:                              REL (Relocatable file)
;;   Machine:                           Intel 80386
;;   Version:                           0x1
;;   Entry point address:               0x0
;;   Start of program headers:          0 (bytes into file)
;;   Start of section headers:          308 (bytes into file)
;;   Flags:                             0x0
;;   Size of this header:               52 (bytes)
;;   Size of program headers:           0 (bytes)
;;   Number of program headers:         0
;;   Size of section headers:           40 (bytes)
;;   Number of section headers:         11
;;   Section header string table index: 8


;; symbol related ---

;; (defmacro slot-extract->flags (object slot extractor  &rest descriptions)
;;   (let ((slot-value-sym (gensym)))
;;     `(let ((,slot-value-sym (funcall ,extractor (slot-value ,object ,slot))))
;;        (remove-nils
;;         (list
;;          ,@(mapcar
;;             #'(lambda (description)
;;                 `(if
;;                   (not (zerop (logand ,slot-value-sym ,(car description))))
;;                   ,@(cdr description)))
;;             descriptions))))))

;; (defun symbol-info-type-keywords (symbol-entry)
;;   (slot-extract->flags symbol-entry 'st-info
;;                        #'(lambda (x) (logand x #XF))
;;                        (+STT_NOTYPE+  :symbol-notype)
;;                        (+STT_OBJECT+  :symbol-data-object)
;;                        (+STT_FUNC+    :symbol-func)
;;                        (+STT_SECTION+ :symbol-section)
;;                        (+STT_FILE+    :symbol-file)
;;                        (+STT_COMMON+  :symbol-common)
;;                        (+STT_TLS+     :symbol-thread-local-storage)))

;; (defun symbol-info-bind-keywords (symbol-entry)
;;   (slot-extract->flags symbol-entry 'st-info
;;                        #'(lambda (x) (ash x -4))
;;                        ;; This info is needed when parsing the symbol table
;;                        (+STB_LOCAL+  :symbol-bind-local)
;;                        (+STB_GLOBAL+ :symbol-bind-global)
;;                        (+STB_WEAK+   :symbol-bind-weak)))

;; (defun symbol-info-keywords (symbol-entry)
;;   (list (symbol-info-type-keywords symbol-entry) 
;;         (symbol-info-bind-keywords symbol-entry)))

;; (defun extract-string (elf string-section-index offset)
;;   "Extract strings from the string section in an elf, identified by
;;    the given section index"
;;   (assert (< string-section-index (length (section-headers-of elf))))
;;   (let*
;;       ((elf-file (file-data-of elf))
;;        (string-section-header 
;;         (nth string-section-index (section-headers-of elf)))
;;        (string-section-offset 
;;         (slot-value string-section-header 'sh-offset)))
;;     (if (not (zerop offset))
;;           (elf-file-excursion 
;;            elf
;;            (+ string-section-offset offset)
;;            (read-value 'asciiz elf-file))
;;           "*unnamed*")))


;; (defun extract-symbol (elf section-index symbol-offset)
;;   (assert (< section-index (length (section-headers-of elf))))
;;   (let ((section-header 
;;          (elt (section-headers-of elf) section-index)))
;;     (let ((section-offset
;;            (slot-value section-header 'sh-offset)))
;;       (elf-file-excursion
;;        elf (+ section-offset symbol-offset)
;;        (read-value 'elf32-sym (file-data-of elf))))))

;; (defclass elf-symbol-table-section (elf-section)
;;   ((string-table-of :initform 0)))

;; (defun dump-symbol-table (elf section-index)
;;   (let 
;;       ((section-header (elt (section-headers-of elf) section-index)))
;;     (let
;;         ((section-offset
;;           (slot-value section-header 'sh-offset))
;;          (section-size
;;           (slot-value section-header 'sh-size))
;;          (section-entsize
;;           (slot-value section-header 'sh-entsize)))
;;       (elf-file-excursion
;;        elf section-offset
;;        (let 
;;            ((symbol-name-extractor 
;;              (make-string-table-extractor 
;;               elf 
;;               (slot-value section-header 'sh-link))))
;;        (loop
;;           for offset = section-offset then (+ offset section-entsize)
;;           until (>= offset (+ section-offset section-size))
;;           do
;;             (let
;;                 ((symbtab-entry
;;                  (read-value 'elf32-sym (file-data-of elf))))
;;               (format t "Pos ~X  value ~A Size ~4,'0X Section ~2,'0X ~%" 
;;                       (buffer-pos-of (file-data-of elf))
;;                       (funcall symbol-name-extractor (slot-value symbtab-entry 'st-name))
;;                       (slot-value symbtab-entry 'st-value)
;;                       (slot-value symbtab-entry 'st-size)
;;                       ;; to do - need flags from st-info
;;                       (slot-value symbtab-entry 'st-shndx))
;;                  ;; now we have symbtab-entry, do something with it
;;               )))))))

;; (defun rel-type (rel-entry)
;;   (logand (slot-value rel-entry 'r-info) #XFF))

;; (defun rel-sym (rel-entry)
;;   (ash (slot-value rel-entry 'r-info) -8))

;; (defun relocation-info-type-keywords (relocation-entry)
;;   (slot-extract->flags relocation-entry 'r-info
;;                        #'rel-type
;;                        (+R_386_NONE+	    :no-relocation)
;;                        (+R_386_32+	        :direct-32-bit)
;;                        (+R_386_PC32+	    :pc-relative-32-bit)
;;                        (+R_386_GOT32+	    :got-entry-32-bit)
;;                        (+R_386_PLT32+	    :plt-address-32bit)
;;                        (+R_386_COPY+	    :copy-symbol-at-runtime)
;;                        (+R_386_GLOB_DAT+   :create-got-entry)
;;                        (+R_386_JMP_SLOT+   :create-plt-entry)
;;                        (+R_386_RELATIVE+   :ajust-by-program-base)
;;                        (+R_386_GOTOFF+	    :got-32-bit-offset)
;;                        (+R_386_GOTPC+	    :got-32-bit-pc-relative-offset)
;;                        (+R_386_32PLT+	    :plt-32-bit)
;;                        (+R_386_TLS_TPOFF+  :static-tls-block-offset)
;;                        (+R_386_TLS_IE+	    :got-entry-address-for-static-tls)
;;                        (+R_386_TLS_GOTIE+	:got-entry-for-static-tls)
;;                        (+R_386_TLS_LE+	    :relative-offset-to-static-tls)
;;                        (+R_386_TLS_GD+	    :gnu-32-general-dynamic-thread-data) 
;;                        (+R_386_TLS_LDM+	:gnu-32-local-dynamic-thread-data)
;;                        (+R_386_16+	        :direct-16-bit)
;;                        (+R_386_PC16+	    :pc-relative-16-bit)
;;                        (+R_386_8+		    :direct-8-bit)
;;                        (+R_386_PC8+	    :pc-relative-8-bit)
;;                        (+R_386_TLS_GD_32+   :direct-32-bit-tls) 
;;                        (+R_386_TLS_GD_PUSH+  :pushl-tls)
;;                        (+R_386_TLS_GD_CALL+  :call-tls)
;;                        (+R_386_TLS_GD_POP+   :popl-tls)
;;                        (+R_386_TLS_LDM_32+   :call-tls-ldw)
;;                        (+R_386_TLS_LDM_PUSH+ :popl-tls-ldw)
;;                        (+R_386_TLS_LDM_CALL+ :call-tls-ldw)
;;                        (+R_386_TLS_LDM_POP+  :popl-tls-ldw)
;;                        (+R_386_TLS_LDO_32+   :tls-relative-offset)
;;                        (+R_386_TLS_IE_32+    :got-neg-tls-offset)
;;                        (+R_386_TLS_LE_32+    :neg-tls-offset)
;;                        (+R_386_TLS_DTPMOD32+ :modue-id)
;;                        (+R_386_TLS_DTPOFF32+ :offset-in-tls)
;;                        (+R_386_TLS_TPOFF32+  :neg-offset-in-tls)))

  
;; (defun dump-relocation-entries (elf section-index &key (addends nil))
;;   (let
;;       ((section-header (elt (section-headers-of elf) section-index)))
;;     (let ((section-offset
;;            (slot-value section-header 'sh-offset))
;;           (section-size
;;            (slot-value section-header 'sh-size))
;;           (section-entsize
;;            (slot-value section-header 'sh-entsize))
;;           (symbol-table-section-index 
;;            (slot-value section-header 'sh-info))
;;           (relocation-section-index 
;;            (slot-value section-header 'sh-link)))
;;       (elf-file-excursion 
;;        elf section-offset
;;        (format t "Relocation Entries: for section ~A using symbol-table ~A~%"
;;                (elt (section-names-of elf) relocation-section-index)
;;                (elt (section-names-of elf) symbol-table-section-index))
;;        (loop
;;           for offset = section-offset then (+ offset section-entsize)
;;           until (>= offset (+ section-offset section-size))
;;           do
;;             (let* 
;;                 ((rel-entry
;;                   (if addends
;;                       (read-value 'elf32-rela-a (file-data-of elf))
;;                       (read-value 'elf32-rela (file-data-of elf))))
;;                  (rel-offset (slot-value rel-entry 'r-offset))
;;                  (rel-symbol-table-index (rel-sym rel-entry))
;;                  (rel-symbol-name-table-index
;;                   (slot-value
;;                    (elt (section-headers-of elf) symbol-table-index)
;;                    'sh-link))
;;                  (rel-symbol
;;                   (extract-symbol elf rel-symbol-table-index (slot-value rel-entry 'r-offset))))
;;             (format t "Pos ~X Offset ~X Sym ~X Type ~X Table ~A Name ~A ~%" 
;;                     (buffer-pos-of (file-data-of elf)) 
;;                     rel-offset
;;                     rel-symbol-table-index
;;                     (rel-type rel-entry)
;;                     (elt (section-names-of elf) rel-symbol-table-index)
;;                     (extract-name elf
;;                                   (slot-value  rel-symbol-name-table-index)
;;                                   (slot-value rel-stbol 'st-name)))))))))
              
;; (defun describe-elf-file-sections (elf-file)
;;   (let
;;       ((elf-header (header-of elf-file)))
;;     ;; etc..
;;     (format t "~A section headers~%" (slot-value elf-header 'e-shnum))
;;     (loop
;;        for section-name in (section-names-of elf-file)
;;        for section-header in (section-headers-of elf-file)
;;        for section-index from 0 below (length (section-headers-of elf-file))
;;        do
;;          (let
;;               ((section-type 
;;                 (section-type->description section-header))
;;                (section-offset
;;                 (slot-value section-header 'sh-offset))
;;                (section-size
;;                 (slot-value section-header 'sh-size))
;;               (section-entsize
;;                (slot-value section-header 'sh-entsize)))
;;            (format t "~%<~A>~%" section-name)
;;            (format t "Type: ~A~%" section-type)
;;            (format t "Flags ~A~%" (section-flags->descriptions section-header))
;;            (format t "Offset ~X Size ~X Entry Size ~X Entries ~X~%"
;;                    section-offset section-size section-entsize
;;                    (if (not (zerop section-entsize))
;;                        (/ section-size section-entsize)
;;                        0))
;;            (case (section-type->keyword section-header)
;;              (:program-data
;;               (if (member :allocate-memory (section-flags->keywords section-header))
;;                   (hexdump-section elf-file section-index)))
;;              (:symbol-table
;;               (dump-symbol-table elf-file section-index))
;;              (:relocation-entries-no-addend
;;               (dump-relocation-entries elf-file section-index))
;;              (:relocation-entries
;;               (dump-relocation-entries elf-file section-index :addends t)))))))
              
        


