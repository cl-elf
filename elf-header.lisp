;;;; Silly emacs, this is -*- Lisp -*-

(in-package :cl-elf)


;; (defmethod describe-elf-header ((self elf-header))
;;   (format t "ELF Header:~&")
;;   (format t "Magic:  ~X ~X ~X ~X ~X ~X ~X ~X ~X ~X ~X ~X ~X ~X ~X ~X ")
;;   (format t "Class: ~A~&") ;;)                             ELF32
;;   (format t "Data@ ~A~&" )
;;   (format t "Version ~A")
;;   (format t "OS/ABI ~A~&")
;;   (format t "Type ~A~&")
;;   (format t "Machine ~A~&")
;;   (format t "Version ~A~&")
;;   (format t "Entry Point Address ~A~&")
;;   (format t "Start of program headers ~A~&")
;;   (format t "Start of section headers ~A~&")
;;   (format t "Flags ~X~&")
;;   (format t "Size of this header ~X~&")
;;   (format t "Size of program headers ~X~&")
;;   (format t "Number of program headers ~X~&")
;;   (format t "Size of section headers ~X~&")
;;   (format t "Number of section headers ~X~&")
;;   (format t "Section header string table index ~X~&"))

;; header ...


(define-elf-class elf-header
    ((e-magic (usb8-array :length +EI_NIDENT+))
     (e-type (elf32-half) )
     (e-machine (elf32-half))
     (e-version (elf32-word))
     (e-entry   (elf32-addr))
     (e-phoff   (elf32-off))
     (e-shoff   (elf32-off))
     (e-flags   (elf32-word))
     (e-ehsize   (elf32-half))
     (e-phentsize   (elf32-half))
     (e-phnum       (elf32-half))
     (e-shentsize   (elf32-half))
     (e-shnum       (elf32-half))
     (e-shstrndx    (elf32-half))))


(define-elf-class elf32-program-header
    ((p-type (u32))
     (p-offset (elf32-off))
     (p-vaddr (elf32-addr))
     (p-paddr (elf32-addr))
     (p_filesz (u32))
     (p_memsz (u32))
     (p_flags (u32))
     (p_align (u32))))

(define-elf-class elf32-section-header
    ((sh-name (u32))
     (sh-type (u32))
     (sh-flags (elf32-addr))
     (sh-addr (elf32-addr))
     (sh-offset (elf32-off))
     (sh-size (u32))
     (sh-link (u32))
     (sh-info (u32))
     (sh-addralign (u32))
     (sh-entsize (u32))))

(define-elf-class elf32-sym
    ((st-name (elf32-word))
     (st-value (elf32-addr))
     (st-size (elf32-word))
     (st-info (u8))
     (st-other (u8))
     (st-shndx (elf32-half))))

(define-elf-class elf32-rela
    ((r-offset (elf32-addr))
     (r-info (u32))))

(define-elf-class elf32-rela-a
    ((r-offset (elf32-addr))
     (r-info (u32))
     (r-addend (s32)))) 
	     
	      
(define-elf-class elf-dyn
    ((d-tag (elf32-sword))
     (d-un (u32))))

;; ;; These constants define the different elf file types 
;; (defconstant +ET_NONE+   0)
;; (defconstant +ET_REL+    1)
;; (defconstant +ET_EXEC+   2)
;; (defconstant +ET_DYN+    3)
;; (defconstant +ET_CORE+   4)
;; (defconstant +ET_LOPROC+ #xff00)
;; (defconstant +ET_HIPROC+ #xffff)

  ;; descriptions go here



;; to do -- alignment?
;;       -- read usb8 arrays
;; little endan -- to do -- some way to switch to big endian eqivalents 

